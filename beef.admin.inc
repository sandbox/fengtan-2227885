<?php

/**
 * @file
 * Admin pages for module BeEF.
 */

/**
 * Config form.
 */
function beef_configuration_form() {
  $form['beef_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#default_value' => variable_get('beef_server_url', 'http://localhost:3000'),
    '#description' => t('BeEF server URL. Default value is http://localhost:3000.'),
  );

  $form['beef_hook_title'] = array(
    '#type' => 'item',
    '#title' => t('Hook settings'),
  );
  $form['beef_hook'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['beef_hook']['role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'beef_hook',
    '#weight' => 10,
  );
  $form['beef_hook']['role']['beef_hooked_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Hook specific roles'),
    '#default_value' => variable_get('beef_hooked_roles', array()),
    '#options' => array_map('check_plain', user_roles()),
    '#description' => t('Hook only the selected role(s). If you select no roles, all users will be hooked.'),
  );
  $form['beef_hook_info'] = array(
    '#type' => 'item',
    '#description' => t('Note: BeEF embeds jQuery 1.5.2. It may cause conflicts with Drupal\'s jQuery library.'),
  );

  // TODO ability to test if hook.js works
  return system_settings_form($form);
}

